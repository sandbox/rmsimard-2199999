/**
 * @file
 * Provides summary information for vertical tabs.
 */

(function ($) {
  Drupal.behaviors.ninja_edit_settings = {
    attach: function() {
      // Provide summary when editting a node.
      $('fieldset#edit-ninja-edit-settings').drupalSetSummary(function() {
        var vals = [];
        if ($('#ninja-edit-enabled').val()) {
          vals.push(Drupal.t('Original timestamps preserved'));
        }
        if (!vals.length) {
          vals.push(Drupal.t('Disabled'));
        }
        return vals.join('<br/>');
      });
      // Provide summary during content type configuration.
      $('fieldset#edit-ninja-edit').drupalSetSummary(function() {
        var vals = [];
        if ($('#edit-ninja-edit-enabled').is(':checked')) {
          vals.push(Drupal.t('Enabled'));
        }
        if (!vals.length) {
          vals.push(Drupal.t('Disabled'));
        }
        return vals.join('<br/>');
      });
    }
  };
}(jQuery));
